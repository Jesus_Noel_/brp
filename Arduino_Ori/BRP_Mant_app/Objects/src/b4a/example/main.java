package b4a.example;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class main extends Activity implements B4AActivity{
	public static main mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = true;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "b4a.example", "b4a.example.main");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (main).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "b4a.example", "b4a.example.main");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "b4a.example.main", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (main) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (main) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (main) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.usb.UsbManagerWrapper _manager = null;
public static anywheresoftware.b4a.objects.usb.UsbDeviceConnectionWrapper _connection = null;
public static anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbEndpointWrapper _outendpoint = null;
public static anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbEndpointWrapper _inendpoint = null;
public static anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbDeviceWrapper _device = null;
public static anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbInterfaceWrapper _interface = null;
public static anywheresoftware.b4a.objects.collections.List _inrequests = null;
public static anywheresoftware.b4a.objects.collections.List _outrequests = null;
public static int _peerid = 0;
public static anywheresoftware.b4a.agraham.byteconverter.ByteConverter _conv = null;
public static String _msg_read = "";
public static String _data_read = "";
public static String[] _arreglo1 = null;
public static anywheresoftware.b4a.objects.UsbSerial _usb = null;
public static anywheresoftware.b4a.randomaccessfile.AsyncStreams _astreams = null;
public anywheresoftware.b4a.objects.EditTextWrapper _edittext1 = null;
public static String _mensaje_out = "";
public anywheresoftware.b4a.objects.LabelWrapper _label1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label6 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label7 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label8 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _togglebutton1 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _togglebutton2 = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _togglebutton3 = null;
public b4a.example.starter _starter = null;

public static boolean isAnyActivityVisible() {
    boolean vis = false;
vis = vis | (main.mostCurrent != null);
return vis;}
public static String  _activity_create(boolean _firsttime) throws Exception{
int _dev = 0;
 //BA.debugLineNum = 90;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 92;BA.debugLine="Activity.LoadLayout(\"Lay1\")";
mostCurrent._activity.LoadLayout("Lay1",mostCurrent.activityBA);
 //BA.debugLineNum = 93;BA.debugLine="Dim dev As Int";
_dev = 0;
 //BA.debugLineNum = 94;BA.debugLine="If (usb.HasPermission(1)) Then	' Ver_2.4";
if ((_usb.HasPermission((int) (1)))) { 
 //BA.debugLineNum = 96;BA.debugLine="Dim dev As Int";
_dev = 0;
 //BA.debugLineNum = 99;BA.debugLine="dev = usb.Open(115200, 1)		' Ver_2.4";
_dev = _usb.Open(processBA,(int) (115200),(int) (1));
 //BA.debugLineNum = 100;BA.debugLine="If dev <> usb.USB_NONE Then";
if (_dev!=_usb.USB_NONE) { 
 //BA.debugLineNum = 105;BA.debugLine="astreams.Initialize(usb.GetInputStream,usb.Get";
_astreams.Initialize(processBA,_usb.GetInputStream(),_usb.GetOutputStream(),"Astreams");
 }else {
 //BA.debugLineNum = 107;BA.debugLine="Log(\"Error opening USB port 1\")";
anywheresoftware.b4a.keywords.Common.Log("Error opening USB port 1");
 };
 }else {
 //BA.debugLineNum = 110;BA.debugLine="usb.RequestPermission(1)  ' Ver_2.4";
_usb.RequestPermission((int) (1));
 };
 //BA.debugLineNum = 119;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 122;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 123;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 120;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 121;BA.debugLine="End Sub";
return "";
}
public static String  _astreams_newdata(byte[] _buffer) throws Exception{
int _intera = 0;
boolean _entra_h = false;
String _command = "";
byte[] _bait = null;
int _command_in = 0;
int _pos_box = 0;
boolean _bandera = false;
int _i = 0;
String _comand1 = "";
String _comand2 = "";
 //BA.debugLineNum = 125;BA.debugLine="Sub Astreams_NewData (Buffer() As Byte)";
 //BA.debugLineNum = 126;BA.debugLine="Dim intera As Int";
_intera = 0;
 //BA.debugLineNum = 127;BA.debugLine="Dim entra_h As Boolean";
_entra_h = false;
 //BA.debugLineNum = 128;BA.debugLine="Dim command As String";
_command = "";
 //BA.debugLineNum = 129;BA.debugLine="Dim BaiT(5) As Byte";
_bait = new byte[(int) (5)];
;
 //BA.debugLineNum = 130;BA.debugLine="Dim command_In As Int";
_command_in = 0;
 //BA.debugLineNum = 131;BA.debugLine="Dim Pos_Box As Int";
_pos_box = 0;
 //BA.debugLineNum = 132;BA.debugLine="Dim bandera As Boolean";
_bandera = false;
 //BA.debugLineNum = 133;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 134;BA.debugLine="Dim comand1, comand2 As String";
_comand1 = "";
_comand2 = "";
 //BA.debugLineNum = 135;BA.debugLine="command = conv.StringFromBytes(Buffer, \"UTF8\")";
_command = _conv.StringFromBytes(_buffer,"UTF8");
 //BA.debugLineNum = 141;BA.debugLine="If command.Trim <> \"\" And command.Trim.Length >";
if ((_command.trim()).equals("") == false && _command.trim().length()>1) { 
 //BA.debugLineNum = 142;BA.debugLine="label8.Text = command";
mostCurrent._label8.setText((Object)(_command));
 };
 //BA.debugLineNum = 145;BA.debugLine="If command <> \"1\" And command <> \"2\" And command <";
if ((_command).equals("1") == false && (_command).equals("2") == false && (_command).equals("3") == false && (_command).equals("4") == false && (_command).equals("5") == false && (_command).equals("6") == false && (_command).equals("7") == false) { 
 //BA.debugLineNum = 147;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 148;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 149;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 150;BA.debugLine="label4.Color = Colors.Transparent";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 151;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 152;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 153;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else {
 //BA.debugLineNum = 155;BA.debugLine="If command = \"1\" Then";
if ((_command).equals("1")) { 
 //BA.debugLineNum = 156;BA.debugLine="Label1.Color = Colors.Green";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 157;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 158;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 159;BA.debugLine="label4.Color = Colors.Transparent";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 160;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 161;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 162;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else if((_command).equals("2")) { 
 //BA.debugLineNum = 164;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 165;BA.debugLine="Label2.Color = Colors.Green";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 166;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 167;BA.debugLine="label4.Color = Colors.Transparent";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 168;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 169;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 170;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else if((_command).equals("3")) { 
 //BA.debugLineNum = 172;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 173;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 174;BA.debugLine="Label3.Color = Colors.Green";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 175;BA.debugLine="label4.Color = Colors.Transparent";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 176;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 177;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 178;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else if((_command).equals("4")) { 
 //BA.debugLineNum = 180;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 181;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 182;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 183;BA.debugLine="label4.Color = Colors.Green";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 184;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 185;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 186;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else if((_command).equals("5")) { 
 //BA.debugLineNum = 188;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 189;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 190;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 191;BA.debugLine="label4.Color = Colors.Green";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 192;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 193;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 194;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else if((_command).equals("6")) { 
 //BA.debugLineNum = 196;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 197;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 198;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 199;BA.debugLine="label4.Color = Colors.Transparent";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 200;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 201;BA.debugLine="label6.Color = Colors.Green";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 202;BA.debugLine="label7.Color = Colors.Transparent";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 }else if((_command).equals("7")) { 
 //BA.debugLineNum = 204;BA.debugLine="Label1.Color = Colors.Transparent";
mostCurrent._label1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 205;BA.debugLine="Label2.Color = Colors.Transparent";
mostCurrent._label2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 206;BA.debugLine="Label3.Color = Colors.Transparent";
mostCurrent._label3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 207;BA.debugLine="label4.Color = Colors.Transparent";
mostCurrent._label4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 208;BA.debugLine="label5.Color = Colors.Transparent";
mostCurrent._label5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 209;BA.debugLine="label6.Color = Colors.Transparent";
mostCurrent._label6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Transparent);
 //BA.debugLineNum = 210;BA.debugLine="label7.Color = Colors.Green";
mostCurrent._label7.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 };
 //BA.debugLineNum = 216;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 82;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 85;BA.debugLine="Dim EditText1 As EditText";
mostCurrent._edittext1 = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 86;BA.debugLine="Dim mensaje_Out As String";
mostCurrent._mensaje_out = "";
 //BA.debugLineNum = 87;BA.debugLine="Dim Label1, Label2, Label3, label4, label5, label6";
mostCurrent._label1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label6 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label7 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label8 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 88;BA.debugLine="Dim Togglebutton1, Togglebutton2, Togglebutton3 As";
mostCurrent._togglebutton1 = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
mostCurrent._togglebutton2 = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
mostCurrent._togglebutton3 = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
 //BA.debugLineNum = 89;BA.debugLine="End Sub";
return "";
}
public static String  _pausa(long _segundos) throws Exception{
long _now = 0L;
 //BA.debugLineNum = 268;BA.debugLine="Sub Pausa(segundos As Long)";
 //BA.debugLineNum = 269;BA.debugLine="Dim now As Long";
_now = 0L;
 //BA.debugLineNum = 270;BA.debugLine="now = DateTime.Now";
_now = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 271;BA.debugLine="Do Until (DateTime.Now > now + (segundos * 1000";
while (!((anywheresoftware.b4a.keywords.Common.DateTime.getNow()>_now+(_segundos*1000)))) {
 //BA.debugLineNum = 272;BA.debugLine="DoEvents";
anywheresoftware.b4a.keywords.Common.DoEvents();
 }
;
 //BA.debugLineNum = 274;BA.debugLine="End Sub";
return "";
}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main._process_globals();
starter._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 15;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 19;BA.debugLine="Dim manager As UsbManager";
_manager = new anywheresoftware.b4a.objects.usb.UsbManagerWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Dim connection As UsbDeviceConnection";
_connection = new anywheresoftware.b4a.objects.usb.UsbDeviceConnectionWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Dim outEndpoint, inEndpoint As UsbEndpoint";
_outendpoint = new anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbEndpointWrapper();
_inendpoint = new anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbEndpointWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Dim device As UsbDevice";
_device = new anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbDeviceWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Dim interface As UsbInterface";
_interface = new anywheresoftware.b4a.objects.usb.UsbManagerWrapper.UsbInterfaceWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Dim InRequests, OutRequests As List";
_inrequests = new anywheresoftware.b4a.objects.collections.List();
_outrequests = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 25;BA.debugLine="Dim peerId As Int";
_peerid = 0;
 //BA.debugLineNum = 26;BA.debugLine="Dim conv As ByteConverter";
_conv = new anywheresoftware.b4a.agraham.byteconverter.ByteConverter();
 //BA.debugLineNum = 27;BA.debugLine="Dim MSG_READ, DATA_READ As String";
_msg_read = "";
_data_read = "";
 //BA.debugLineNum = 28;BA.debugLine="MSG_READ = \"Msg-Read\" : DATA_READ = \"Data-Read\"";
_msg_read = "Msg-Read";
 //BA.debugLineNum = 28;BA.debugLine="MSG_READ = \"Msg-Read\" : DATA_READ = \"Data-Read\"";
_data_read = "Data-Read";
 //BA.debugLineNum = 29;BA.debugLine="Dim Arreglo1(49) As String";
_arreglo1 = new String[(int) (49)];
java.util.Arrays.fill(_arreglo1,"");
 //BA.debugLineNum = 30;BA.debugLine="Dim usb As UsbSerial";
_usb = new anywheresoftware.b4a.objects.UsbSerial();
 //BA.debugLineNum = 31;BA.debugLine="Dim astreams As AsyncStreams";
_astreams = new anywheresoftware.b4a.randomaccessfile.AsyncStreams();
 //BA.debugLineNum = 32;BA.debugLine="Arreglo1(1) = \"g\"";
_arreglo1[(int) (1)] = "g";
 //BA.debugLineNum = 33;BA.debugLine="Arreglo1(2) = \"n\"";
_arreglo1[(int) (2)] = "n";
 //BA.debugLineNum = 34;BA.debugLine="Arreglo1(3) = \"u\"";
_arreglo1[(int) (3)] = "u";
 //BA.debugLineNum = 35;BA.debugLine="Arreglo1(4) = \"D\"";
_arreglo1[(int) (4)] = "D";
 //BA.debugLineNum = 36;BA.debugLine="Arreglo1(5) = \"K\"";
_arreglo1[(int) (5)] = "K";
 //BA.debugLineNum = 37;BA.debugLine="Arreglo1(6) = \"R\"";
_arreglo1[(int) (6)] = "R";
 //BA.debugLineNum = 38;BA.debugLine="Arreglo1(7) = \"f\"";
_arreglo1[(int) (7)] = "f";
 //BA.debugLineNum = 39;BA.debugLine="Arreglo1(8) = \"m\"";
_arreglo1[(int) (8)] = "m";
 //BA.debugLineNum = 40;BA.debugLine="Arreglo1(9) = \"t\"";
_arreglo1[(int) (9)] = "t";
 //BA.debugLineNum = 41;BA.debugLine="Arreglo1(10) = \"C\"";
_arreglo1[(int) (10)] = "C";
 //BA.debugLineNum = 42;BA.debugLine="Arreglo1(11) = \"J\"";
_arreglo1[(int) (11)] = "J";
 //BA.debugLineNum = 43;BA.debugLine="Arreglo1(12) = \"Q\"";
_arreglo1[(int) (12)] = "Q";
 //BA.debugLineNum = 44;BA.debugLine="Arreglo1(13) = \"X\"";
_arreglo1[(int) (13)] = "X";
 //BA.debugLineNum = 45;BA.debugLine="Arreglo1(14) = \"e\"";
_arreglo1[(int) (14)] = "e";
 //BA.debugLineNum = 46;BA.debugLine="Arreglo1(15) = \"l\"";
_arreglo1[(int) (15)] = "l";
 //BA.debugLineNum = 47;BA.debugLine="Arreglo1(16) = \"s\"";
_arreglo1[(int) (16)] = "s";
 //BA.debugLineNum = 48;BA.debugLine="Arreglo1(17) = \"B\"";
_arreglo1[(int) (17)] = "B";
 //BA.debugLineNum = 49;BA.debugLine="Arreglo1(18) = \"I\"";
_arreglo1[(int) (18)] = "I";
 //BA.debugLineNum = 50;BA.debugLine="Arreglo1(19) = \"P\"";
_arreglo1[(int) (19)] = "P";
 //BA.debugLineNum = 51;BA.debugLine="Arreglo1(20) = \"Q\"";
_arreglo1[(int) (20)] = "Q";
 //BA.debugLineNum = 52;BA.debugLine="Arreglo1(21) = \"d\"";
_arreglo1[(int) (21)] = "d";
 //BA.debugLineNum = 53;BA.debugLine="Arreglo1(22) = \"k\"";
_arreglo1[(int) (22)] = "k";
 //BA.debugLineNum = 54;BA.debugLine="Arreglo1(23) = \"r\"";
_arreglo1[(int) (23)] = "r";
 //BA.debugLineNum = 55;BA.debugLine="Arreglo1(24) = \"A\"";
_arreglo1[(int) (24)] = "A";
 //BA.debugLineNum = 56;BA.debugLine="Arreglo1(25) = \"H\"";
_arreglo1[(int) (25)] = "H";
 //BA.debugLineNum = 57;BA.debugLine="Arreglo1(26) = \"O\"";
_arreglo1[(int) (26)] = "O";
 //BA.debugLineNum = 58;BA.debugLine="Arreglo1(27) = \"V\"";
_arreglo1[(int) (27)] = "V";
 //BA.debugLineNum = 59;BA.debugLine="Arreglo1(28) = \"c\"";
_arreglo1[(int) (28)] = "c";
 //BA.debugLineNum = 60;BA.debugLine="Arreglo1(29) = \"j\"";
_arreglo1[(int) (29)] = "j";
 //BA.debugLineNum = 61;BA.debugLine="Arreglo1(30) = \"q\"";
_arreglo1[(int) (30)] = "q";
 //BA.debugLineNum = 62;BA.debugLine="Arreglo1(31) = \"x\"";
_arreglo1[(int) (31)] = "x";
 //BA.debugLineNum = 63;BA.debugLine="Arreglo1(32) = \"G\"";
_arreglo1[(int) (32)] = "G";
 //BA.debugLineNum = 64;BA.debugLine="Arreglo1(33) = \"N\"";
_arreglo1[(int) (33)] = "N";
 //BA.debugLineNum = 65;BA.debugLine="Arreglo1(34) = \"U\"";
_arreglo1[(int) (34)] = "U";
 //BA.debugLineNum = 66;BA.debugLine="Arreglo1(35) = \"b\"";
_arreglo1[(int) (35)] = "b";
 //BA.debugLineNum = 67;BA.debugLine="Arreglo1(36) = \"i\"";
_arreglo1[(int) (36)] = "i";
 //BA.debugLineNum = 68;BA.debugLine="Arreglo1(37) = \"p\"";
_arreglo1[(int) (37)] = "p";
 //BA.debugLineNum = 69;BA.debugLine="Arreglo1(38) = \"w\"";
_arreglo1[(int) (38)] = "w";
 //BA.debugLineNum = 70;BA.debugLine="Arreglo1(39) = \"F\"";
_arreglo1[(int) (39)] = "F";
 //BA.debugLineNum = 71;BA.debugLine="Arreglo1(40) = \"M\"";
_arreglo1[(int) (40)] = "M";
 //BA.debugLineNum = 72;BA.debugLine="Arreglo1(41) = \"T\"";
_arreglo1[(int) (41)] = "T";
 //BA.debugLineNum = 73;BA.debugLine="Arreglo1(42) = \"a\"";
_arreglo1[(int) (42)] = "a";
 //BA.debugLineNum = 74;BA.debugLine="Arreglo1(43) = \"h\"";
_arreglo1[(int) (43)] = "h";
 //BA.debugLineNum = 75;BA.debugLine="Arreglo1(44) = \"o\"";
_arreglo1[(int) (44)] = "o";
 //BA.debugLineNum = 76;BA.debugLine="Arreglo1(45) = \"v\"";
_arreglo1[(int) (45)] = "v";
 //BA.debugLineNum = 77;BA.debugLine="Arreglo1(46) = \"E\"";
_arreglo1[(int) (46)] = "E";
 //BA.debugLineNum = 78;BA.debugLine="Arreglo1(47) = \"L\"";
_arreglo1[(int) (47)] = "L";
 //BA.debugLineNum = 79;BA.debugLine="Arreglo1(48) = \"S\"";
_arreglo1[(int) (48)] = "S";
 //BA.debugLineNum = 80;BA.debugLine="End Sub";
return "";
}
public static String  _togglebutton1_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 233;BA.debugLine="Sub ToggleButton1_CheckedChange(Checked As Boolean";
 //BA.debugLineNum = 234;BA.debugLine="If Checked = True Then";
if (_checked==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 235;BA.debugLine="mensaje_Out = \";\"";
mostCurrent._mensaje_out = ";";
 //BA.debugLineNum = 236;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 }else {
 //BA.debugLineNum = 238;BA.debugLine="mensaje_Out = \":\"";
mostCurrent._mensaje_out = ":";
 //BA.debugLineNum = 239;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 };
 //BA.debugLineNum = 241;BA.debugLine="End Sub";
return "";
}
public static String  _togglebutton2_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 221;BA.debugLine="Sub ToggleButton2_CheckedChange(Checked As Boolean";
 //BA.debugLineNum = 222;BA.debugLine="If Checked = False Then";
if (_checked==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 223;BA.debugLine="If EditText1.Text <>\"\" And (EditText1.Text > 0 A";
if ((mostCurrent._edittext1.getText()).equals("") == false && ((double)(Double.parseDouble(mostCurrent._edittext1.getText()))>0 && (double)(Double.parseDouble(mostCurrent._edittext1.getText()))<49)) { 
 //BA.debugLineNum = 224;BA.debugLine="mensaje_Out = Arreglo1(EditText1.Text)";
mostCurrent._mensaje_out = _arreglo1[(int)(Double.parseDouble(mostCurrent._edittext1.getText()))];
 //BA.debugLineNum = 225;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 };
 }else {
 //BA.debugLineNum = 228;BA.debugLine="mensaje_Out = \"-\"";
mostCurrent._mensaje_out = "-";
 //BA.debugLineNum = 229;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 };
 //BA.debugLineNum = 231;BA.debugLine="End Sub";
return "";
}
public static String  _togglebutton3_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 243;BA.debugLine="Sub ToggleButton3_CheckedChange(Checked As Boolean";
 //BA.debugLineNum = 245;BA.debugLine="If Checked = False Then";
if (_checked==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 247;BA.debugLine="mensaje_Out = \"Y\"";
mostCurrent._mensaje_out = "Y";
 //BA.debugLineNum = 248;BA.debugLine="Try";
try { //BA.debugLineNum = 249;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 } 
       catch (Exception e6) {
			processBA.setLastException(e6); //BA.debugLineNum = 251;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 //BA.debugLineNum = 252;BA.debugLine="Msgbox(LastException,\"lol\")";
anywheresoftware.b4a.keywords.Common.Msgbox(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)),"lol",mostCurrent.activityBA);
 };
 //BA.debugLineNum = 254;BA.debugLine="Togglebutton3.Color = Colors.Gray";
mostCurrent._togglebutton3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Gray);
 }else {
 //BA.debugLineNum = 257;BA.debugLine="mensaje_Out = \"Z\"";
mostCurrent._mensaje_out = "Z";
 //BA.debugLineNum = 258;BA.debugLine="Try";
try { //BA.debugLineNum = 259;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 } 
       catch (Exception e15) {
			processBA.setLastException(e15); //BA.debugLineNum = 261;BA.debugLine="Log(LastException)";
anywheresoftware.b4a.keywords.Common.Log(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)));
 //BA.debugLineNum = 262;BA.debugLine="Msgbox(LastException,\"lol\")";
anywheresoftware.b4a.keywords.Common.Msgbox(BA.ObjectToString(anywheresoftware.b4a.keywords.Common.LastException(mostCurrent.activityBA)),"lol",mostCurrent.activityBA);
 };
 //BA.debugLineNum = 264;BA.debugLine="Togglebutton3.Color = Colors.red";
mostCurrent._togglebutton3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Red);
 };
 //BA.debugLineNum = 266;BA.debugLine="End Sub";
return "";
}
public static String  _write_in_arduino(String _ms) throws Exception{
 //BA.debugLineNum = 217;BA.debugLine="Sub Write_In_Arduino (Ms As String)";
 //BA.debugLineNum = 218;BA.debugLine="Ms = mensaje_Out";
_ms = mostCurrent._mensaje_out;
 //BA.debugLineNum = 219;BA.debugLine="astreams.Write(Ms.GetBytes(\"UTF8\"))";
_astreams.Write(_ms.getBytes("UTF8"));
 //BA.debugLineNum = 220;BA.debugLine="End Sub";
return "";
}
}
