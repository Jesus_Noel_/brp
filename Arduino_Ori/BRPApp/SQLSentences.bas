﻿Type=StaticCode
Version=5.8
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
  Dim SQL1 As SQL
  Type SQLSentences(first As Boolean)
End Sub
Sub init ()
	 	
	 	'SQL1.Initialize("/storage/emulated/0/Android/data", "Decals1.db", True)
		 SQL1.Initialize(File.DirDefaultExternal, "Decals1.db", True)
End Sub
Sub CreateTables()
   ' SQL1.ExecNonQuery("DROP TABLE IF EXISTS Table1")
   ' SQL1.ExecNonQuery("DROP TABLE IF EXISTS table2")
   'SQL1.ExecNonQuery("CREATE TABLE table1 (Box VARCAHR(255) , Decal_PN VARCAHR(255), Figure VARCAHR(255), # VARCAHR(255), Number VARCAHR(255), Number_2 VARCAHR(255), Model VARCAHR(255), QR_Req VARCAHR(255))")
     SQL1.ExecNonQuery("CREATE TABLE IF NOT EXISTS Table1 (Box TEXT , Decal_PN TEXT, Figure TEXT, Ca TEXT, Number TEXT, Number_2 TEXT, Model TEXT, QR_Req TEXT, Color TEXT)")
	 'SQL1.ExecNonQuery("CREATE TABLE IF NOT EXISTS table1 (Modelo TEXT , E1 TEXT, E2 TEXT)")
  
    'SQL1.ExecNonQuery("CREATE TABLE table2 (name TEXT, image BLOB)")
End Sub


Sub FillSimpleData
    SQL1.ExecNonQuery("INSERT INTO table1 VALUES('abc', 1, 2)")
    SQL1.ExecNonQuery2("INSERT INTO table1 VALUES(?, ?, ?)", Array As Object("def", 3, 4))
End Sub

Sub LogTable1
    Dim Cursor1 As Cursor
	Public dr1, dr2, dr3 As String 
    Cursor1 = SQL1.ExecQuery("SELECT col1, col2, col3 FROM table1")
    For i = 0 To Cursor1.RowCount - 1
        Cursor1.Position = i
        Log("************************")
        Log(Cursor1.GetString("col1"))
		Log(Cursor1.Getstring("col2"))
        Log(Cursor1.Getstring("col3"))
		dr1 = Cursor1.GetString("col1")
		dr2 = Cursor1.GetString("col2")
		dr3 = Cursor1.GetString("col3")
    Next
    Cursor1.Close
End Sub
Sub InsertManyRows
    SQL1.BeginTransaction
    Try
        For i = 1 To 500
            SQL1.ExecNonQuery2("INSERT INTO table1 VALUES ('def', ?, ?)", Array As Object(i, i))
        Next
        SQL1.TransactionSuccessful
    Catch
        Log(LastException.Message)
    End Try
    SQL1.EndTransaction
End Sub
Sub insert (Box As String, Decal_PN As String, Figure As String, cat As String,  Number As String,  Number_2 As String,  Model As String,  QR_Req As String)
	  SQL1.ExecNonQuery2("INSERT INTO table1 VALUES(?, ?, ?, ?, ?, ?, ?, ?)", Array As Object(Box, Decal_PN, Figure, cat, Number,Number_2, Model, QR_Req))

End Sub
Sub read 
End Sub

