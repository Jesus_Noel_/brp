package BRP_1.example;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class main extends Activity implements B4AActivity{
	public static main mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = true;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "BRP_1.example", "BRP_1.example.main");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (main).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
        WaitForLayout wl = new WaitForLayout();
        if (anywheresoftware.b4a.objects.ServiceHelper.StarterHelper.startFromActivity(processBA, wl, false))
		    BA.handler.postDelayed(wl, 5);

	}
	static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "BRP_1.example", "BRP_1.example.main");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "BRP_1.example.main", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (main) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (main) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (main) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.sql.SQL _sql1 = null;
public static anywheresoftware.b4a.agraham.byteconverter.ByteConverter _conv = null;
public static anywheresoftware.b4a.objects.UsbSerial _usb = null;
public static anywheresoftware.b4a.randomaccessfile.AsyncStreams _astreams = null;
public static String[] _arreglo1 = null;
public static String[] _qr_code = null;
public static int _howmqr = 0;
public static String[] _qr_escaneados = null;
public static int _generic_qr_count_bd = 0;
public static int _generic_qr_count_scann = 0;
public static String _scanned_to_check = "";
public static boolean _codigo_in_label = false;
public static boolean _codigo_eliminado_label = false;
public anywheresoftware.b4a.objects.ImageViewWrapper _imageview1 = null;
public anywheresoftware.b4a.objects.WorkbookWrapper _workbook1 = null;
public anywheresoftware.b4a.objects.WorkbookWrapper.SheetWrapper _moviessheet = null;
public anywheresoftware.b4a.objects.WorkbookWrapper.WritableWorkbookWrapper _write = null;
public static int _i = 0;
public static String[] _dr1 = null;
public static String[] _dr2 = null;
public static String[] _dr3 = null;
public static String[] _dr4 = null;
public static String[] _dr5 = null;
public static String[] _dr6 = null;
public static String[] _dr7 = null;
public static String[] _dr8 = null;
public BRP_1.example.table _table1 = null;
public static int _howmled = 0;
public static int _howmmsg = 0;
public anywheresoftware.b4a.objects.EditTextWrapper _edittext_data_scanner_recived = null;
public anywheresoftware.b4a.objects.EditTextWrapper _edittext2 = null;
public static String _box = "";
public static String _decal_pn = "";
public static String _figure = "";
public static String _cat = "";
public static String _number = "";
public static String _number_2 = "";
public static String _model = "";
public static String _qr_req = "";
public anywheresoftware.b4a.objects.ButtonWrapper _importbtn = null;
public anywheresoftware.b4a.objects.ButtonWrapper _exportbtn = null;
public anywheresoftware.b4a.objects.LabelWrapper _label1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _l1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _l2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _l3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _l4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _l5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _l6 = null;
public anywheresoftware.b4a.objects.LabelWrapper _box1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _box2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _box3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _box4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _box5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _box6 = null;
public anywheresoftware.b4a.objects.LabelWrapper _qr1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _qr2 = null;
public anywheresoftware.b4a.objects.LabelWrapper _qr3 = null;
public anywheresoftware.b4a.objects.LabelWrapper _qr4 = null;
public anywheresoftware.b4a.objects.LabelWrapper _qr5 = null;
public anywheresoftware.b4a.objects.LabelWrapper _qr6 = null;
public static String _needqr = "";
public static String _mensaje_out = "";
public static boolean _start = false;
public BRP_1.example.starter _starter = null;
public BRP_1.example.sqlsentences _sqlsentences = null;

public static boolean isAnyActivityVisible() {
    boolean vis = false;
vis = vis | (main.mostCurrent != null);
return vis;}
public static String  _activity_create(boolean _firsttime) throws Exception{
int _dev = 0;
 //BA.debugLineNum = 113;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 115;BA.debugLine="Activity.LoadLayout(\"Lay1\")";
mostCurrent._activity.LoadLayout("Lay1",mostCurrent.activityBA);
 //BA.debugLineNum = 116;BA.debugLine="HowMMsg = 0";
_howmmsg = (int) (0);
 //BA.debugLineNum = 117;BA.debugLine="Start = True";
_start = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 118;BA.debugLine="Dim dev As Int";
_dev = 0;
 //BA.debugLineNum = 119;BA.debugLine="If (usb.HasPermission(1)) Then	' Ver_2.4";
if ((_usb.HasPermission((int) (1)))) { 
 //BA.debugLineNum = 121;BA.debugLine="Dim dev As Int";
_dev = 0;
 //BA.debugLineNum = 124;BA.debugLine="dev = usb.Open(115200, 1)		' Ver_2.4";
_dev = _usb.Open(processBA,(int) (115200),(int) (1));
 //BA.debugLineNum = 125;BA.debugLine="If dev <> usb.USB_NONE Then";
if (_dev!=_usb.USB_NONE) { 
 //BA.debugLineNum = 130;BA.debugLine="astreams.Initialize(usb.GetInputStream,usb.Get";
_astreams.Initialize(processBA,_usb.GetInputStream(),_usb.GetOutputStream(),"Astreams");
 }else {
 //BA.debugLineNum = 132;BA.debugLine="Log(\"Error opening USB port 1\")";
anywheresoftware.b4a.keywords.Common.Log("Error opening USB port 1");
 };
 }else {
 //BA.debugLineNum = 135;BA.debugLine="usb.RequestPermission(1)  ' Ver_2.4";
_usb.RequestPermission((int) (1));
 };
 //BA.debugLineNum = 139;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 140;BA.debugLine="SQL1.Initialize(File.DirDefaultExternal, \"";
_sql1.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirDefaultExternal(),"Decals1.db",anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 142;BA.debugLine="SQLSentences.init";
mostCurrent._sqlsentences._init(mostCurrent.activityBA);
 //BA.debugLineNum = 143;BA.debugLine="SQLSentences.CreateTables";
mostCurrent._sqlsentences._createtables(mostCurrent.activityBA);
 //BA.debugLineNum = 144;BA.debugLine="EditText_Data_Scanner_Recived.RequestFocus";
mostCurrent._edittext_data_scanner_recived.RequestFocus();
 //BA.debugLineNum = 145;BA.debugLine="End Sub";
return "";
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 151;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 153;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
 //BA.debugLineNum = 147;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 149;BA.debugLine="End Sub";
return "";
}
public static String  _astreams_newdata(byte[] _buffer) throws Exception{
 //BA.debugLineNum = 279;BA.debugLine="Sub Astreams_NewData (Buffer() As Byte)";
 //BA.debugLineNum = 314;BA.debugLine="End Sub";
return "";
}
public static String  _button1_click() throws Exception{
 //BA.debugLineNum = 632;BA.debugLine="Sub Button1_Click";
 //BA.debugLineNum = 633;BA.debugLine="If usb.Open(9600,1) Then";
if (BA.ObjectToBoolean(_usb.Open(processBA,(int) (9600),(int) (1)))) { 
 //BA.debugLineNum = 635;BA.debugLine="astreams.Initialize(usb.GetInputStream,usb.GetOu";
_astreams.Initialize(processBA,_usb.GetInputStream(),_usb.GetOutputStream(),"Astreams");
 //BA.debugLineNum = 636;BA.debugLine="Pausa(2)";
_pausa((long) (2));
 //BA.debugLineNum = 637;BA.debugLine="mensaje_Out = \"Z\"";
mostCurrent._mensaje_out = "Z";
 //BA.debugLineNum = 638;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 }else {
 };
 //BA.debugLineNum = 642;BA.debugLine="End Sub";
return "";
}
public static String  _edittext_data_scanner_recived_enterpressed() throws Exception{
boolean _bandera2 = false;
boolean _bande3 = false;
int _clean_qr_scanned = 0;
 //BA.debugLineNum = 326;BA.debugLine="Sub EditText_Data_Scanner_Recived_EnterPressed";
 //BA.debugLineNum = 327;BA.debugLine="If EditText_Data_Scanner_Recived.Text = \"0000\" Th";
if ((mostCurrent._edittext_data_scanner_recived.getText()).equals("0000")) { 
 //BA.debugLineNum = 328;BA.debugLine="Start = True";
_start = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 329;BA.debugLine="Label2.Text = \"Reinicio\"";
mostCurrent._label2.setText((Object)("Reinicio"));
 //BA.debugLineNum = 330;BA.debugLine="Label1.Text = \"\"";
mostCurrent._label1.setText((Object)(""));
 //BA.debugLineNum = 331;BA.debugLine="label4.Text = \"\"";
mostCurrent._label4.setText((Object)(""));
 //BA.debugLineNum = 332;BA.debugLine="label5.Text = \"Escanee Modelo\"";
mostCurrent._label5.setText((Object)("Escanee Modelo"));
 //BA.debugLineNum = 333;BA.debugLine="L1.Text = \"\"";
mostCurrent._l1.setText((Object)(""));
 //BA.debugLineNum = 334;BA.debugLine="Box1.Text = \"\"";
mostCurrent._box1.setText((Object)(""));
 //BA.debugLineNum = 335;BA.debugLine="QR1.Text = \"\"";
mostCurrent._qr1.setText((Object)(""));
 //BA.debugLineNum = 338;BA.debugLine="L2.Text =\"\"";
mostCurrent._l2.setText((Object)(""));
 //BA.debugLineNum = 339;BA.debugLine="Box2.Text = \"\"";
mostCurrent._box2.setText((Object)(""));
 //BA.debugLineNum = 340;BA.debugLine="QR2.Text = \"\"";
mostCurrent._qr2.setText((Object)(""));
 //BA.debugLineNum = 342;BA.debugLine="L3.Text =\"\"";
mostCurrent._l3.setText((Object)(""));
 //BA.debugLineNum = 343;BA.debugLine="Box3.Text = \"\"";
mostCurrent._box3.setText((Object)(""));
 //BA.debugLineNum = 344;BA.debugLine="QR3.Text = \"\"";
mostCurrent._qr3.setText((Object)(""));
 //BA.debugLineNum = 347;BA.debugLine="L4.Text = \"\"";
mostCurrent._l4.setText((Object)(""));
 //BA.debugLineNum = 348;BA.debugLine="Box4.Text =\"\"";
mostCurrent._box4.setText((Object)(""));
 //BA.debugLineNum = 349;BA.debugLine="QR4.Text = \"\"";
mostCurrent._qr4.setText((Object)(""));
 //BA.debugLineNum = 352;BA.debugLine="L5.Text = \"\"";
mostCurrent._l5.setText((Object)(""));
 //BA.debugLineNum = 353;BA.debugLine="Box5.Text = \"\"";
mostCurrent._box5.setText((Object)(""));
 //BA.debugLineNum = 354;BA.debugLine="QR5.Text = \"\"";
mostCurrent._qr5.setText((Object)(""));
 //BA.debugLineNum = 357;BA.debugLine="L6.Text = \"\"";
mostCurrent._l6.setText((Object)(""));
 //BA.debugLineNum = 358;BA.debugLine="Box6.Text = \"\"";
mostCurrent._box6.setText((Object)(""));
 //BA.debugLineNum = 359;BA.debugLine="QR6.Text = \"\"";
mostCurrent._qr6.setText((Object)(""));
 //BA.debugLineNum = 361;BA.debugLine="Box1.Color = Colors.White";
mostCurrent._box1.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 362;BA.debugLine="L1.Color = Colors.White";
mostCurrent._l1.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 363;BA.debugLine="QR1.Color = Colors.White";
mostCurrent._qr1.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 367;BA.debugLine="Box2.Color = Colors.White";
mostCurrent._box2.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 368;BA.debugLine="L2.Color = Colors.White";
mostCurrent._l2.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 369;BA.debugLine="QR2.Color = Colors.White";
mostCurrent._qr2.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 372;BA.debugLine="Box3.Color = Colors.White";
mostCurrent._box3.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 373;BA.debugLine="L3.Color = Colors.White";
mostCurrent._l3.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 374;BA.debugLine="QR3.Color = Colors.White";
mostCurrent._qr3.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 377;BA.debugLine="Box4.Color = Colors.White";
mostCurrent._box4.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 378;BA.debugLine="L4.Color = Colors.White";
mostCurrent._l4.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 379;BA.debugLine="QR4.Color = Colors.White";
mostCurrent._qr4.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 382;BA.debugLine="Box5.Color = Colors.White";
mostCurrent._box5.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 383;BA.debugLine="L5.Color = Colors.White";
mostCurrent._l5.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 384;BA.debugLine="QR5.Color = Colors.White";
mostCurrent._qr5.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 387;BA.debugLine="Box6.Color = Colors.White";
mostCurrent._box6.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 388;BA.debugLine="L6.Color = Colors.White";
mostCurrent._l6.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 389;BA.debugLine="QR6.Color = Colors.White";
mostCurrent._qr6.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 392;BA.debugLine="For i = 0 To 19";
{
final int step43 = 1;
final int limit43 = (int) (19);
for (_i = (int) (0) ; (step43 > 0 && _i <= limit43) || (step43 < 0 && _i >= limit43); _i = ((int)(0 + _i + step43)) ) {
 //BA.debugLineNum = 393;BA.debugLine="QR_Code(i)=\"\"";
_qr_code[_i] = "";
 //BA.debugLineNum = 394;BA.debugLine="QR_Escaneados(i) = \"\"";
_qr_escaneados[_i] = "";
 }
};
 //BA.debugLineNum = 396;BA.debugLine="HowMQR = 0";
_howmqr = (int) (0);
 //BA.debugLineNum = 397;BA.debugLine="mensaje_Out=\"Z\"";
mostCurrent._mensaje_out = "Z";
 //BA.debugLineNum = 398;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 //BA.debugLineNum = 399;BA.debugLine="Activity.Color = Colors.RGB(255, 255, 255)";
mostCurrent._activity.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 400;BA.debugLine="EditText_Data_Scanner_Recived.SelectAll";
mostCurrent._edittext_data_scanner_recived.SelectAll();
 }else {
 //BA.debugLineNum = 403;BA.debugLine="If Start = True  Then";
if (_start==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 406;BA.debugLine="If EditText_Data_Scanner_Recived.Text <> \" \" Th";
if ((mostCurrent._edittext_data_scanner_recived.getText()).equals(" ") == false) { 
 //BA.debugLineNum = 407;BA.debugLine="Box1.Color = Colors.White";
mostCurrent._box1.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 408;BA.debugLine="L1.Color = Colors.White";
mostCurrent._l1.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 409;BA.debugLine="QR1.Color = Colors.White";
mostCurrent._qr1.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 413;BA.debugLine="Box2.Color = Colors.White";
mostCurrent._box2.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 414;BA.debugLine="L2.Color = Colors.White";
mostCurrent._l2.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 415;BA.debugLine="QR2.Color = Colors.White";
mostCurrent._qr2.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 418;BA.debugLine="Box3.Color = Colors.White";
mostCurrent._box3.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 419;BA.debugLine="L3.Color = Colors.White";
mostCurrent._l3.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 420;BA.debugLine="QR3.Color = Colors.White";
mostCurrent._qr3.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 423;BA.debugLine="Box4.Color = Colors.White";
mostCurrent._box4.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 424;BA.debugLine="L4.Color = Colors.White";
mostCurrent._l4.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 425;BA.debugLine="QR4.Color = Colors.White";
mostCurrent._qr4.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 428;BA.debugLine="Box5.Color = Colors.White";
mostCurrent._box5.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 429;BA.debugLine="L5.Color = Colors.White";
mostCurrent._l5.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 430;BA.debugLine="QR5.Color = Colors.White";
mostCurrent._qr5.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 433;BA.debugLine="Box6.Color = Colors.White";
mostCurrent._box6.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 434;BA.debugLine="L6.Color = Colors.White";
mostCurrent._l6.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 435;BA.debugLine="QR6.Color = Colors.White";
mostCurrent._qr6.setColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 436;BA.debugLine="Generic_QR_count_BD = 0";
_generic_qr_count_bd = (int) (0);
 //BA.debugLineNum = 437;BA.debugLine="Generic_QR_count_Scann = 0";
_generic_qr_count_scann = (int) (0);
 //BA.debugLineNum = 438;BA.debugLine="Read(EditText_Data_Scanner_Recived.Text)";
_read(mostCurrent._edittext_data_scanner_recived.getText());
 //BA.debugLineNum = 439;BA.debugLine="mensaje_Out=\"Y\"";
mostCurrent._mensaje_out = "Y";
 //BA.debugLineNum = 440;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 //BA.debugLineNum = 441;BA.debugLine="label5.Text = EditText_Data_Scanner_Recived.Te";
mostCurrent._label5.setText((Object)(mostCurrent._edittext_data_scanner_recived.getText()));
 //BA.debugLineNum = 442;BA.debugLine="EditText_Data_Scanner_Recived.SelectAll";
mostCurrent._edittext_data_scanner_recived.SelectAll();
 //BA.debugLineNum = 443;BA.debugLine="Activity.Color = Colors.RGB(255, 255, 255)";
mostCurrent._activity.setColor(anywheresoftware.b4a.keywords.Common.Colors.RGB((int) (255),(int) (255),(int) (255)));
 //BA.debugLineNum = 444;BA.debugLine="Start = False";
_start = anywheresoftware.b4a.keywords.Common.False;
 };
 }else {
 //BA.debugLineNum = 447;BA.debugLine="Dim bandera2, bande3 As Boolean";
_bandera2 = false;
_bande3 = false;
 //BA.debugLineNum = 448;BA.debugLine="Dim Clean_Qr_Scanned As Int";
_clean_qr_scanned = 0;
 //BA.debugLineNum = 449;BA.debugLine="bande3 = True";
_bande3 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 451;BA.debugLine="If EditText_Data_Scanner_Recived.Text <> \" \" Th";
if ((mostCurrent._edittext_data_scanner_recived.getText()).equals(" ") == false) { 
 //BA.debugLineNum = 452;BA.debugLine="If EditText_Data_Scanner_Recived.Text.Length >";
if (mostCurrent._edittext_data_scanner_recived.getText().length()>4) { 
 //BA.debugLineNum = 453;BA.debugLine="Scanned_To_Check = EditText_Data_Scanner_Reci";
_scanned_to_check = mostCurrent._edittext_data_scanner_recived.getText();
 }else {
 //BA.debugLineNum = 455;BA.debugLine="Generic_QR_count_Scann = Generic_QR_count_Sca";
_generic_qr_count_scann = (int) (_generic_qr_count_scann+1);
 //BA.debugLineNum = 456;BA.debugLine="Scanned_To_Check = EditText_Data_Scanner_Reci";
_scanned_to_check = mostCurrent._edittext_data_scanner_recived.getText()+anywheresoftware.b4a.keywords.Common.NumberFormat(_generic_qr_count_scann,(int) (0),(int) (0));
 };
 //BA.debugLineNum = 460;BA.debugLine="For i = 0 To 20";
{
final int step94 = 1;
final int limit94 = (int) (20);
for (_i = (int) (0) ; (step94 > 0 && _i <= limit94) || (step94 < 0 && _i >= limit94); _i = ((int)(0 + _i + step94)) ) {
 //BA.debugLineNum = 462;BA.debugLine="If (Scanned_To_Check = QR_Code(i))  Then";
if (((_scanned_to_check).equals(_qr_code[_i]))) { 
 //BA.debugLineNum = 465;BA.debugLine="For Clean_Qr_Scanned = 0 To 20";
{
final int step96 = 1;
final int limit96 = (int) (20);
for (_clean_qr_scanned = (int) (0) ; (step96 > 0 && _clean_qr_scanned <= limit96) || (step96 < 0 && _clean_qr_scanned >= limit96); _clean_qr_scanned = ((int)(0 + _clean_qr_scanned + step96)) ) {
 //BA.debugLineNum = 466;BA.debugLine="If Scanned_To_Check <> QR_Escaneados(Clean_";
if ((_scanned_to_check).equals(_qr_escaneados[_clean_qr_scanned]) == false) { 
 //BA.debugLineNum = 467;BA.debugLine="bande3 = True";
_bande3 = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 471;BA.debugLine="If Clean_Qr_Scanned = 20 Then";
if (_clean_qr_scanned==20) { 
 //BA.debugLineNum = 472;BA.debugLine="QR_Escaneados(i) = Scanned_To_Check";
_qr_escaneados[_i] = _scanned_to_check;
 };
 }else {
 //BA.debugLineNum = 476;BA.debugLine="bande3 = False";
_bande3 = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 477;BA.debugLine="Clean_Qr_Scanned = 20";
_clean_qr_scanned = (int) (20);
 };
 }
};
 //BA.debugLineNum = 480;BA.debugLine="If bande3 = True Then";
if (_bande3==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 481;BA.debugLine="If HowMQR<>0 Then";
if (_howmqr!=0) { 
 //BA.debugLineNum = 482;BA.debugLine="Codigo_Eliminado_Label = False";
_codigo_eliminado_label = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 483;BA.debugLine="HowMQR = HowMQR - 1";
_howmqr = (int) (_howmqr-1);
 //BA.debugLineNum = 484;BA.debugLine="Label2.Text = \"Etiquetas Por Escanear: \" &";
mostCurrent._label2.setText((Object)("Etiquetas Por Escanear: "+BA.NumberToString(_howmqr)));
 //BA.debugLineNum = 485;BA.debugLine="If L1.Text = EditText_Data_Scanner_Recived";
if ((mostCurrent._l1.getText()).equals(mostCurrent._edittext_data_scanner_recived.getText())) { 
 //BA.debugLineNum = 486;BA.debugLine="Box1.Color = Colors.Green";
mostCurrent._box1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 487;BA.debugLine="L1.Color = Colors.Green";
mostCurrent._l1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 488;BA.debugLine="QR1.Color = Colors.Green";
mostCurrent._qr1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 //BA.debugLineNum = 491;BA.debugLine="If L2.Text = EditText_Data_Scanner_Recived";
if ((mostCurrent._l2.getText()).equals(mostCurrent._edittext_data_scanner_recived.getText())) { 
 //BA.debugLineNum = 492;BA.debugLine="Box2.Color = Colors.Green";
mostCurrent._box2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 493;BA.debugLine="L2.Color = Colors.Green";
mostCurrent._l2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 494;BA.debugLine="QR2.Color = Colors.Green";
mostCurrent._qr2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 //BA.debugLineNum = 497;BA.debugLine="If L3.Text = EditText_Data_Scanner_Recived";
if ((mostCurrent._l3.getText()).equals(mostCurrent._edittext_data_scanner_recived.getText())) { 
 //BA.debugLineNum = 498;BA.debugLine="Box3.Color = Colors.Green";
mostCurrent._box3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 499;BA.debugLine="L3.Color = Colors.Green";
mostCurrent._l3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 500;BA.debugLine="QR3.Color = Colors.Green";
mostCurrent._qr3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 //BA.debugLineNum = 503;BA.debugLine="If L4.Text = EditText_Data_Scanner_Recived";
if ((mostCurrent._l4.getText()).equals(mostCurrent._edittext_data_scanner_recived.getText())) { 
 //BA.debugLineNum = 504;BA.debugLine="Box4.Color = Colors.Green";
mostCurrent._box4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 505;BA.debugLine="L4.Color = Colors.Green";
mostCurrent._l4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 506;BA.debugLine="QR4.Color = Colors.Green";
mostCurrent._qr4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 //BA.debugLineNum = 509;BA.debugLine="If L5.Text = EditText_Data_Scanner_Recived";
if ((mostCurrent._l5.getText()).equals(mostCurrent._edittext_data_scanner_recived.getText())) { 
 //BA.debugLineNum = 510;BA.debugLine="Box5.Color = Colors.Green";
mostCurrent._box5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 511;BA.debugLine="L5.Color = Colors.Green";
mostCurrent._l5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 512;BA.debugLine="QR5.Color = Colors.Green";
mostCurrent._qr5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 //BA.debugLineNum = 515;BA.debugLine="If L6.Text = EditText_Data_Scanner_Recived";
if ((mostCurrent._l6.getText()).equals(mostCurrent._edittext_data_scanner_recived.getText())) { 
 //BA.debugLineNum = 516;BA.debugLine="Box6.Color = Colors.Green";
mostCurrent._box6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 517;BA.debugLine="L6.Color = Colors.Green";
mostCurrent._l6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 518;BA.debugLine="QR6.Color = Colors.Green";
mostCurrent._qr6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 };
 //BA.debugLineNum = 523;BA.debugLine="EditText_Data_Scanner_Recived.Text = \"\"";
mostCurrent._edittext_data_scanner_recived.setText((Object)(""));
 //BA.debugLineNum = 524;BA.debugLine="i = 20";
_i = (int) (20);
 //BA.debugLineNum = 526;BA.debugLine="If HowMQR = 0 Then";
if (_howmqr==0) { 
 //BA.debugLineNum = 527;BA.debugLine="Generic_QR_count_Scann = 0";
_generic_qr_count_scann = (int) (0);
 //BA.debugLineNum = 529;BA.debugLine="L1.Text = \"\"";
mostCurrent._l1.setText((Object)(""));
 //BA.debugLineNum = 530;BA.debugLine="Box1.Text = \"\"";
mostCurrent._box1.setText((Object)(""));
 //BA.debugLineNum = 531;BA.debugLine="QR1.Text = \"\"";
mostCurrent._qr1.setText((Object)(""));
 //BA.debugLineNum = 534;BA.debugLine="L2.Text =\"\"";
mostCurrent._l2.setText((Object)(""));
 //BA.debugLineNum = 535;BA.debugLine="Box2.Text = \"\"";
mostCurrent._box2.setText((Object)(""));
 //BA.debugLineNum = 536;BA.debugLine="QR2.Text = \"\"";
mostCurrent._qr2.setText((Object)(""));
 //BA.debugLineNum = 538;BA.debugLine="L3.Text =\"\"";
mostCurrent._l3.setText((Object)(""));
 //BA.debugLineNum = 539;BA.debugLine="Box3.Text = \"\"";
mostCurrent._box3.setText((Object)(""));
 //BA.debugLineNum = 540;BA.debugLine="QR3.Text = \"\"";
mostCurrent._qr3.setText((Object)(""));
 //BA.debugLineNum = 543;BA.debugLine="L4.Text = \"\"";
mostCurrent._l4.setText((Object)(""));
 //BA.debugLineNum = 544;BA.debugLine="Box4.Text =\"\"";
mostCurrent._box4.setText((Object)(""));
 //BA.debugLineNum = 545;BA.debugLine="QR4.Text = \"\"";
mostCurrent._qr4.setText((Object)(""));
 //BA.debugLineNum = 548;BA.debugLine="L5.Text = \"\"";
mostCurrent._l5.setText((Object)(""));
 //BA.debugLineNum = 549;BA.debugLine="Box5.Text = \"\"";
mostCurrent._box5.setText((Object)(""));
 //BA.debugLineNum = 550;BA.debugLine="QR5.Text = \"\"";
mostCurrent._qr5.setText((Object)(""));
 //BA.debugLineNum = 553;BA.debugLine="L6.Text = \"\"";
mostCurrent._l6.setText((Object)(""));
 //BA.debugLineNum = 554;BA.debugLine="Box6.Text = \"\"";
mostCurrent._box6.setText((Object)(""));
 //BA.debugLineNum = 555;BA.debugLine="QR6.Text = \"\"";
mostCurrent._qr6.setText((Object)(""));
 //BA.debugLineNum = 556;BA.debugLine="Box1.Color = Colors.Green";
mostCurrent._box1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 557;BA.debugLine="L1.Color = Colors.Green";
mostCurrent._l1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 558;BA.debugLine="QR1.Color = Colors.Green";
mostCurrent._qr1.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 562;BA.debugLine="Box2.Color = Colors.Green";
mostCurrent._box2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 563;BA.debugLine="L2.Color = Colors.Green";
mostCurrent._l2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 564;BA.debugLine="QR2.Color = Colors.Green";
mostCurrent._qr2.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 567;BA.debugLine="Box3.Color = Colors.Green";
mostCurrent._box3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 568;BA.debugLine="L3.Color = Colors.Green";
mostCurrent._l3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 569;BA.debugLine="QR3.Color = Colors.Green";
mostCurrent._qr3.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 572;BA.debugLine="Box4.Color = Colors.Green";
mostCurrent._box4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 573;BA.debugLine="L4.Color = Colors.Green";
mostCurrent._l4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 574;BA.debugLine="QR4.Color = Colors.Green";
mostCurrent._qr4.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 577;BA.debugLine="Box5.Color = Colors.Green";
mostCurrent._box5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 578;BA.debugLine="L5.Color = Colors.Green";
mostCurrent._l5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 579;BA.debugLine="QR5.Color = Colors.Green";
mostCurrent._qr5.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 582;BA.debugLine="Box6.Color = Colors.Green";
mostCurrent._box6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 583;BA.debugLine="L6.Color = Colors.Green";
mostCurrent._l6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 584;BA.debugLine="QR6.Color = Colors.Green";
mostCurrent._qr6.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 586;BA.debugLine="Activity.Color = Colors.Green";
mostCurrent._activity.setColor(anywheresoftware.b4a.keywords.Common.Colors.Green);
 //BA.debugLineNum = 588;BA.debugLine="Label2.Text =\"\"";
mostCurrent._label2.setText((Object)(""));
 //BA.debugLineNum = 589;BA.debugLine="Label2.Text = \"Pieza Terminada\"";
mostCurrent._label2.setText((Object)("Pieza Terminada"));
 //BA.debugLineNum = 590;BA.debugLine="mensaje_Out=\"Z\"";
mostCurrent._mensaje_out = "Z";
 //BA.debugLineNum = 591;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 //BA.debugLineNum = 593;BA.debugLine="For Clean_Qr_Scanned = 0 To 20";
{
final int step187 = 1;
final int limit187 = (int) (20);
for (_clean_qr_scanned = (int) (0) ; (step187 > 0 && _clean_qr_scanned <= limit187) || (step187 < 0 && _clean_qr_scanned >= limit187); _clean_qr_scanned = ((int)(0 + _clean_qr_scanned + step187)) ) {
 //BA.debugLineNum = 594;BA.debugLine="QR_Escaneados(Clean_Qr_Scanned) = \"\"";
_qr_escaneados[_clean_qr_scanned] = "";
 }
};
 //BA.debugLineNum = 596;BA.debugLine="Start = True";
_start = anywheresoftware.b4a.keywords.Common.True;
 };
 }else {
 };
 }else {
 //BA.debugLineNum = 602;BA.debugLine="Label2.Text = \"Etiqueta Repetida\"";
mostCurrent._label2.setText((Object)("Etiqueta Repetida"));
 //BA.debugLineNum = 603;BA.debugLine="mensaje_Out = \"B\"";
mostCurrent._mensaje_out = "B";
 //BA.debugLineNum = 604;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 //BA.debugLineNum = 608;BA.debugLine="i = 20";
_i = (int) (20);
 };
 }else if(((_scanned_to_check).equals(_qr_code[_i]) == false) && (_i==20)) { 
 //BA.debugLineNum = 612;BA.debugLine="Label2.Text = \"Etiqueta Incorrecta\"";
mostCurrent._label2.setText((Object)("Etiqueta Incorrecta"));
 //BA.debugLineNum = 613;BA.debugLine="mensaje_Out = \"B\"";
mostCurrent._mensaje_out = "B";
 //BA.debugLineNum = 614;BA.debugLine="Write_In_Arduino(mensaje_Out)";
_write_in_arduino(mostCurrent._mensaje_out);
 };
 }
};
 //BA.debugLineNum = 622;BA.debugLine="EditText_Data_Scanner_Recived.SelectAll";
mostCurrent._edittext_data_scanner_recived.SelectAll();
 };
 };
 };
 //BA.debugLineNum = 628;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 91;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 94;BA.debugLine="Dim imageview1 As ImageView";
mostCurrent._imageview1 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 95;BA.debugLine="Dim workbook1 As ReadableWorkbook";
mostCurrent._workbook1 = new anywheresoftware.b4a.objects.WorkbookWrapper();
 //BA.debugLineNum = 96;BA.debugLine="Dim moviesSheet As ReadableSheet";
mostCurrent._moviessheet = new anywheresoftware.b4a.objects.WorkbookWrapper.SheetWrapper();
 //BA.debugLineNum = 97;BA.debugLine="Dim write As WritableWorkbook";
mostCurrent._write = new anywheresoftware.b4a.objects.WorkbookWrapper.WritableWorkbookWrapper();
 //BA.debugLineNum = 98;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 99;BA.debugLine="Dim dr1(20), dr2(20), dr3(20), dr4(20), dr5(20),";
mostCurrent._dr1 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr1,"");
mostCurrent._dr2 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr2,"");
mostCurrent._dr3 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr3,"");
mostCurrent._dr4 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr4,"");
mostCurrent._dr5 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr5,"");
mostCurrent._dr6 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr6,"");
mostCurrent._dr7 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr7,"");
mostCurrent._dr8 = new String[(int) (20)];
java.util.Arrays.fill(mostCurrent._dr8,"");
 //BA.debugLineNum = 100;BA.debugLine="Dim table1 As Table";
mostCurrent._table1 = new BRP_1.example.table();
 //BA.debugLineNum = 101;BA.debugLine="Dim HowMLed As Int";
_howmled = 0;
 //BA.debugLineNum = 102;BA.debugLine="Dim HowMMsg As Int";
_howmmsg = 0;
 //BA.debugLineNum = 103;BA.debugLine="Dim EditText_Data_Scanner_Recived, EditText2 As E";
mostCurrent._edittext_data_scanner_recived = new anywheresoftware.b4a.objects.EditTextWrapper();
mostCurrent._edittext2 = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 104;BA.debugLine="Dim Box, Decal_PN, Figure, cat, Number, Number_2,";
mostCurrent._box = "";
mostCurrent._decal_pn = "";
mostCurrent._figure = "";
mostCurrent._cat = "";
mostCurrent._number = "";
mostCurrent._number_2 = "";
mostCurrent._model = "";
mostCurrent._qr_req = "";
 //BA.debugLineNum = 105;BA.debugLine="Dim ImportBtn, ExportBtn As Button";
mostCurrent._importbtn = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._exportbtn = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 106;BA.debugLine="Dim Label1, Label2, Label3, label4, label5, L1, L";
mostCurrent._label1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._l1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._l2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._l3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._l4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._l5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._l6 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 107;BA.debugLine="Dim Box1, Box2, Box3, Box4, Box5, Box6, QR1, QR2,";
mostCurrent._box1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._box2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._box3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._box4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._box5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._box6 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._qr1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._qr2 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._qr3 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._qr4 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._qr5 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._qr6 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 108;BA.debugLine="Dim NeedQR As String";
mostCurrent._needqr = "";
 //BA.debugLineNum = 109;BA.debugLine="Dim mensaje_Out As String";
mostCurrent._mensaje_out = "";
 //BA.debugLineNum = 110;BA.debugLine="Dim Start As Boolean";
_start = false;
 //BA.debugLineNum = 111;BA.debugLine="End Sub";
return "";
}
public static String  _pausa(long _segundos) throws Exception{
long _now = 0L;
 //BA.debugLineNum = 155;BA.debugLine="Sub Pausa(segundos As Long)";
 //BA.debugLineNum = 156;BA.debugLine="Dim now As Long";
_now = 0L;
 //BA.debugLineNum = 157;BA.debugLine="now = DateTime.Now";
_now = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 158;BA.debugLine="Do Until (DateTime.Now > now + (segundos * 1000";
while (!((anywheresoftware.b4a.keywords.Common.DateTime.getNow()>_now+(_segundos*1000)))) {
 //BA.debugLineNum = 159;BA.debugLine="DoEvents";
anywheresoftware.b4a.keywords.Common.DoEvents();
 }
;
 //BA.debugLineNum = 161;BA.debugLine="End Sub";
return "";
}

public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main._process_globals();
starter._process_globals();
sqlsentences._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 15;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 18;BA.debugLine="Dim SQL1 As SQL";
_sql1 = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 19;BA.debugLine="Dim conv As ByteConverter";
_conv = new anywheresoftware.b4a.agraham.byteconverter.ByteConverter();
 //BA.debugLineNum = 20;BA.debugLine="Dim usb As UsbSerial";
_usb = new anywheresoftware.b4a.objects.UsbSerial();
 //BA.debugLineNum = 21;BA.debugLine="Dim astreams As AsyncStreams";
_astreams = new anywheresoftware.b4a.randomaccessfile.AsyncStreams();
 //BA.debugLineNum = 23;BA.debugLine="Dim Arreglo1(56) As String";
_arreglo1 = new String[(int) (56)];
java.util.Arrays.fill(_arreglo1,"");
 //BA.debugLineNum = 24;BA.debugLine="Dim QR_Code(21) As String";
_qr_code = new String[(int) (21)];
java.util.Arrays.fill(_qr_code,"");
 //BA.debugLineNum = 25;BA.debugLine="Dim HowMQR As Int";
_howmqr = 0;
 //BA.debugLineNum = 26;BA.debugLine="Dim QR_Escaneados(21) As String";
_qr_escaneados = new String[(int) (21)];
java.util.Arrays.fill(_qr_escaneados,"");
 //BA.debugLineNum = 27;BA.debugLine="Dim Generic_QR_count_BD As Int";
_generic_qr_count_bd = 0;
 //BA.debugLineNum = 28;BA.debugLine="Dim Generic_QR_count_Scann As Int";
_generic_qr_count_scann = 0;
 //BA.debugLineNum = 29;BA.debugLine="Dim Scanned_To_Check As String";
_scanned_to_check = "";
 //BA.debugLineNum = 30;BA.debugLine="Dim Codigo_In_Label As Boolean";
_codigo_in_label = false;
 //BA.debugLineNum = 31;BA.debugLine="Dim Codigo_Eliminado_Label As Boolean";
_codigo_eliminado_label = false;
 //BA.debugLineNum = 33;BA.debugLine="Arreglo1(1) = \"g\"";
_arreglo1[(int) (1)] = "g";
 //BA.debugLineNum = 34;BA.debugLine="Arreglo1(2) = \"n\"";
_arreglo1[(int) (2)] = "n";
 //BA.debugLineNum = 35;BA.debugLine="Arreglo1(3) = \"u\"";
_arreglo1[(int) (3)] = "u";
 //BA.debugLineNum = 36;BA.debugLine="Arreglo1(4) = \"D\"";
_arreglo1[(int) (4)] = "D";
 //BA.debugLineNum = 37;BA.debugLine="Arreglo1(5) = \"K\"";
_arreglo1[(int) (5)] = "K";
 //BA.debugLineNum = 38;BA.debugLine="Arreglo1(6) = \"R\"";
_arreglo1[(int) (6)] = "R";
 //BA.debugLineNum = 39;BA.debugLine="Arreglo1(7) = \"f\"";
_arreglo1[(int) (7)] = "f";
 //BA.debugLineNum = 40;BA.debugLine="Arreglo1(8) = \"m\"";
_arreglo1[(int) (8)] = "m";
 //BA.debugLineNum = 41;BA.debugLine="Arreglo1(9) = \"t\"";
_arreglo1[(int) (9)] = "t";
 //BA.debugLineNum = 42;BA.debugLine="Arreglo1(10) = \"C\"";
_arreglo1[(int) (10)] = "C";
 //BA.debugLineNum = 43;BA.debugLine="Arreglo1(11) = \"J\"";
_arreglo1[(int) (11)] = "J";
 //BA.debugLineNum = 44;BA.debugLine="Arreglo1(12) = \"Q\"";
_arreglo1[(int) (12)] = "Q";
 //BA.debugLineNum = 45;BA.debugLine="Arreglo1(13) = \"X\"";
_arreglo1[(int) (13)] = "X";
 //BA.debugLineNum = 46;BA.debugLine="Arreglo1(14) = \"e\"";
_arreglo1[(int) (14)] = "e";
 //BA.debugLineNum = 47;BA.debugLine="Arreglo1(15) = \"l\"";
_arreglo1[(int) (15)] = "l";
 //BA.debugLineNum = 48;BA.debugLine="Arreglo1(16) = \"s\"";
_arreglo1[(int) (16)] = "s";
 //BA.debugLineNum = 49;BA.debugLine="Arreglo1(17) = \"B\"";
_arreglo1[(int) (17)] = "B";
 //BA.debugLineNum = 50;BA.debugLine="Arreglo1(18) = \"I\"";
_arreglo1[(int) (18)] = "I";
 //BA.debugLineNum = 51;BA.debugLine="Arreglo1(19) = \"P\"";
_arreglo1[(int) (19)] = "P";
 //BA.debugLineNum = 52;BA.debugLine="Arreglo1(20) = \"W\"";
_arreglo1[(int) (20)] = "W";
 //BA.debugLineNum = 53;BA.debugLine="Arreglo1(21) = \"d\"";
_arreglo1[(int) (21)] = "d";
 //BA.debugLineNum = 54;BA.debugLine="Arreglo1(22) = \"k\"";
_arreglo1[(int) (22)] = "k";
 //BA.debugLineNum = 55;BA.debugLine="Arreglo1(23) = \"r\"";
_arreglo1[(int) (23)] = "r";
 //BA.debugLineNum = 56;BA.debugLine="Arreglo1(24) = \"A\"";
_arreglo1[(int) (24)] = "A";
 //BA.debugLineNum = 57;BA.debugLine="Arreglo1(25) = \"H\"";
_arreglo1[(int) (25)] = "H";
 //BA.debugLineNum = 58;BA.debugLine="Arreglo1(26) = \"O\"";
_arreglo1[(int) (26)] = "O";
 //BA.debugLineNum = 59;BA.debugLine="Arreglo1(27) = \"V\"";
_arreglo1[(int) (27)] = "V";
 //BA.debugLineNum = 60;BA.debugLine="Arreglo1(28) = \"c\"";
_arreglo1[(int) (28)] = "c";
 //BA.debugLineNum = 61;BA.debugLine="Arreglo1(29) = \"j\"";
_arreglo1[(int) (29)] = "j";
 //BA.debugLineNum = 62;BA.debugLine="Arreglo1(30) = \"q\"";
_arreglo1[(int) (30)] = "q";
 //BA.debugLineNum = 63;BA.debugLine="Arreglo1(31) = \"x\"";
_arreglo1[(int) (31)] = "x";
 //BA.debugLineNum = 64;BA.debugLine="Arreglo1(32) = \"G\"";
_arreglo1[(int) (32)] = "G";
 //BA.debugLineNum = 65;BA.debugLine="Arreglo1(33) = \"N\"";
_arreglo1[(int) (33)] = "N";
 //BA.debugLineNum = 66;BA.debugLine="Arreglo1(34) = \"U\"";
_arreglo1[(int) (34)] = "U";
 //BA.debugLineNum = 67;BA.debugLine="Arreglo1(35) = \"b\"";
_arreglo1[(int) (35)] = "b";
 //BA.debugLineNum = 68;BA.debugLine="Arreglo1(36) = \"i\"";
_arreglo1[(int) (36)] = "i";
 //BA.debugLineNum = 69;BA.debugLine="Arreglo1(37) = \"p\"";
_arreglo1[(int) (37)] = "p";
 //BA.debugLineNum = 70;BA.debugLine="Arreglo1(38) = \"w\"";
_arreglo1[(int) (38)] = "w";
 //BA.debugLineNum = 71;BA.debugLine="Arreglo1(39) = \"F\"";
_arreglo1[(int) (39)] = "F";
 //BA.debugLineNum = 72;BA.debugLine="Arreglo1(40) = \"M\"";
_arreglo1[(int) (40)] = "M";
 //BA.debugLineNum = 73;BA.debugLine="Arreglo1(41) = \"T\"";
_arreglo1[(int) (41)] = "T";
 //BA.debugLineNum = 74;BA.debugLine="Arreglo1(42) = \"a\"";
_arreglo1[(int) (42)] = "a";
 //BA.debugLineNum = 75;BA.debugLine="Arreglo1(43) = \"h\"";
_arreglo1[(int) (43)] = "h";
 //BA.debugLineNum = 76;BA.debugLine="Arreglo1(44) = \"o\"";
_arreglo1[(int) (44)] = "o";
 //BA.debugLineNum = 77;BA.debugLine="Arreglo1(45) = \"v\"";
_arreglo1[(int) (45)] = "v";
 //BA.debugLineNum = 78;BA.debugLine="Arreglo1(46) = \"E\"";
_arreglo1[(int) (46)] = "E";
 //BA.debugLineNum = 79;BA.debugLine="Arreglo1(47) = \"L\"";
_arreglo1[(int) (47)] = "L";
 //BA.debugLineNum = 80;BA.debugLine="Arreglo1(48) = \"S\"";
_arreglo1[(int) (48)] = "S";
 //BA.debugLineNum = 82;BA.debugLine="Arreglo1(49) = \"1\"";
_arreglo1[(int) (49)] = "1";
 //BA.debugLineNum = 83;BA.debugLine="Arreglo1(50) = \"2\"";
_arreglo1[(int) (50)] = "2";
 //BA.debugLineNum = 84;BA.debugLine="Arreglo1(51) = \"3\"";
_arreglo1[(int) (51)] = "3";
 //BA.debugLineNum = 85;BA.debugLine="Arreglo1(52) = \"4\"";
_arreglo1[(int) (52)] = "4";
 //BA.debugLineNum = 86;BA.debugLine="Arreglo1(53) = \"5\"";
_arreglo1[(int) (53)] = "5";
 //BA.debugLineNum = 87;BA.debugLine="Arreglo1(54) = \"6\"";
_arreglo1[(int) (54)] = "6";
 //BA.debugLineNum = 88;BA.debugLine="Arreglo1(55) = \"7\"";
_arreglo1[(int) (55)] = "7";
 //BA.debugLineNum = 89;BA.debugLine="End Sub";
return "";
}
public static String  _read(String _in_st) throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor1 = null;
byte[] _buffer = null;
String _msgout = "";
 //BA.debugLineNum = 165;BA.debugLine="Sub Read (In_St As String)";
 //BA.debugLineNum = 166;BA.debugLine="Dim Cursor1 As Cursor";
_cursor1 = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 167;BA.debugLine="Dim buffer() As Byte";
_buffer = new byte[(int) (0)];
;
 //BA.debugLineNum = 168;BA.debugLine="Dim msgOut As String";
_msgout = "";
 //BA.debugLineNum = 169;BA.debugLine="Codigo_In_Label = False";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 170;BA.debugLine="For i = 0 To 19";
{
final int step5 = 1;
final int limit5 = (int) (19);
for (_i = (int) (0) ; (step5 > 0 && _i <= limit5) || (step5 < 0 && _i >= limit5); _i = ((int)(0 + _i + step5)) ) {
 //BA.debugLineNum = 171;BA.debugLine="QR_Code(i)=\"\"";
_qr_code[_i] = "";
 }
};
 //BA.debugLineNum = 174;BA.debugLine="Cursor1 = SQL1.ExecQuery(\"SELECT Box, Decal_PN, F";
_cursor1.setObject((android.database.Cursor)(_sql1.ExecQuery("SELECT Box, Decal_PN, Figure, Ca, Number, Number_2, QR_Req FROM Table1 where Model = '"+_in_st+"'")));
 //BA.debugLineNum = 175;BA.debugLine="For i = 0 To Cursor1.RowCount - 1";
{
final int step9 = 1;
final int limit9 = (int) (_cursor1.getRowCount()-1);
for (_i = (int) (0) ; (step9 > 0 && _i <= limit9) || (step9 < 0 && _i >= limit9); _i = ((int)(0 + _i + step9)) ) {
 //BA.debugLineNum = 176;BA.debugLine="Cursor1.Position = i";
_cursor1.setPosition(_i);
 //BA.debugLineNum = 177;BA.debugLine="dr1(i) = Cursor1.GetString(\"Box\")";
mostCurrent._dr1[_i] = _cursor1.GetString("Box");
 //BA.debugLineNum = 178;BA.debugLine="dr2(i) = Cursor1.GetString(\"Decal_PN\")";
mostCurrent._dr2[_i] = _cursor1.GetString("Decal_PN");
 //BA.debugLineNum = 179;BA.debugLine="dr3(i) = Cursor1.GetString(\"Figure\")";
mostCurrent._dr3[_i] = _cursor1.GetString("Figure");
 //BA.debugLineNum = 180;BA.debugLine="dr4(i) = Cursor1.GetString(\"Ca\")";
mostCurrent._dr4[_i] = _cursor1.GetString("Ca");
 //BA.debugLineNum = 181;BA.debugLine="dr5(i) = Cursor1.GetString(\"Number\")";
mostCurrent._dr5[_i] = _cursor1.GetString("Number");
 //BA.debugLineNum = 182;BA.debugLine="dr6(i) = Cursor1.GetString(\"Number_2\")";
mostCurrent._dr6[_i] = _cursor1.GetString("Number_2");
 //BA.debugLineNum = 183;BA.debugLine="dr7(i) = Cursor1.GetString(\"QR_Req\")";
mostCurrent._dr7[_i] = _cursor1.GetString("QR_Req");
 //BA.debugLineNum = 184;BA.debugLine="QR_Code(i) = dr2(i)";
_qr_code[_i] = mostCurrent._dr2[_i];
 //BA.debugLineNum = 185;BA.debugLine="If dr7(i) = \"1\" Then";
if ((mostCurrent._dr7[_i]).equals("1")) { 
 //BA.debugLineNum = 186;BA.debugLine="NeedQR = \"SI\"";
mostCurrent._needqr = "SI";
 //BA.debugLineNum = 187;BA.debugLine="HowMQR = HowMQR + 1";
_howmqr = (int) (_howmqr+1);
 //BA.debugLineNum = 188;BA.debugLine="QR_Code(i) = dr2(i)";
_qr_code[_i] = mostCurrent._dr2[_i];
 }else {
 //BA.debugLineNum = 190;BA.debugLine="NeedQR = \"NO\"";
mostCurrent._needqr = "NO";
 };
 //BA.debugLineNum = 193;BA.debugLine="If L1.Text = \"\" And Codigo_In_Label = False The";
if ((mostCurrent._l1.getText()).equals("") && _codigo_in_label==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 194;BA.debugLine="L1.Text = QR_Code(i)";
mostCurrent._l1.setText((Object)(_qr_code[_i]));
 //BA.debugLineNum = 195;BA.debugLine="Box1.Text = dr1(i)";
mostCurrent._box1.setText((Object)(mostCurrent._dr1[_i]));
 //BA.debugLineNum = 196;BA.debugLine="QR1.Text = NeedQR";
mostCurrent._qr1.setText((Object)(mostCurrent._needqr));
 //BA.debugLineNum = 197;BA.debugLine="Codigo_In_Label = True";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 199;BA.debugLine="If L2.Text = \"\" And Codigo_In_Label = False The";
if ((mostCurrent._l2.getText()).equals("") && _codigo_in_label==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 200;BA.debugLine="L2.Text = QR_Code(i)";
mostCurrent._l2.setText((Object)(_qr_code[_i]));
 //BA.debugLineNum = 201;BA.debugLine="Box2.Text = dr1(i)";
mostCurrent._box2.setText((Object)(mostCurrent._dr1[_i]));
 //BA.debugLineNum = 202;BA.debugLine="QR2.Text = NeedQR";
mostCurrent._qr2.setText((Object)(mostCurrent._needqr));
 //BA.debugLineNum = 203;BA.debugLine="Codigo_In_Label = True";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 205;BA.debugLine="If L3.Text = \"\" And Codigo_In_Label = False The";
if ((mostCurrent._l3.getText()).equals("") && _codigo_in_label==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 206;BA.debugLine="L3.Text = QR_Code(i)";
mostCurrent._l3.setText((Object)(_qr_code[_i]));
 //BA.debugLineNum = 207;BA.debugLine="Box3.Text = dr1(i)";
mostCurrent._box3.setText((Object)(mostCurrent._dr1[_i]));
 //BA.debugLineNum = 208;BA.debugLine="QR3.Text = NeedQR";
mostCurrent._qr3.setText((Object)(mostCurrent._needqr));
 //BA.debugLineNum = 209;BA.debugLine="Codigo_In_Label = True";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 211;BA.debugLine="If L4.Text = \"\" And Codigo_In_Label = False The";
if ((mostCurrent._l4.getText()).equals("") && _codigo_in_label==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 212;BA.debugLine="L4.Text = QR_Code(i)";
mostCurrent._l4.setText((Object)(_qr_code[_i]));
 //BA.debugLineNum = 213;BA.debugLine="Box4.Text = dr1(i)";
mostCurrent._box4.setText((Object)(mostCurrent._dr1[_i]));
 //BA.debugLineNum = 214;BA.debugLine="QR4.Text = NeedQR";
mostCurrent._qr4.setText((Object)(mostCurrent._needqr));
 //BA.debugLineNum = 215;BA.debugLine="Codigo_In_Label = True";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 217;BA.debugLine="If L5.Text = \"\" And Codigo_In_Label = False The";
if ((mostCurrent._l5.getText()).equals("") && _codigo_in_label==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 218;BA.debugLine="L5.Text = QR_Code(i)";
mostCurrent._l5.setText((Object)(_qr_code[_i]));
 //BA.debugLineNum = 219;BA.debugLine="Box5.Text = dr1(i)";
mostCurrent._box5.setText((Object)(mostCurrent._dr1[_i]));
 //BA.debugLineNum = 220;BA.debugLine="QR5.Text = NeedQR";
mostCurrent._qr5.setText((Object)(mostCurrent._needqr));
 //BA.debugLineNum = 221;BA.debugLine="Codigo_In_Label = True";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 223;BA.debugLine="If L6.Text = \"\" And Codigo_In_Label = False The";
if ((mostCurrent._l6.getText()).equals("") && _codigo_in_label==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 224;BA.debugLine="L6.Text = QR_Code(i)";
mostCurrent._l6.setText((Object)(_qr_code[_i]));
 //BA.debugLineNum = 225;BA.debugLine="Box6.Text = dr1(i)";
mostCurrent._box6.setText((Object)(mostCurrent._dr1[_i]));
 //BA.debugLineNum = 226;BA.debugLine="QR6.Text = NeedQR";
mostCurrent._qr6.setText((Object)(mostCurrent._needqr));
 //BA.debugLineNum = 227;BA.debugLine="Codigo_In_Label = True";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 265;BA.debugLine="Codigo_In_Label = False";
_codigo_in_label = anywheresoftware.b4a.keywords.Common.False;
 }
};
 //BA.debugLineNum = 268;BA.debugLine="Label2.Text = \"Etiquetas Por Escanear: \" & HowMQR";
mostCurrent._label2.setText((Object)("Etiquetas Por Escanear: "+BA.NumberToString(_howmqr)));
 //BA.debugLineNum = 270;BA.debugLine="Label1.text = dr5(0)";
mostCurrent._label1.setText((Object)(mostCurrent._dr5[(int) (0)]));
 //BA.debugLineNum = 271;BA.debugLine="label4.Text = dr3(0)";
mostCurrent._label4.setText((Object)(mostCurrent._dr3[(int) (0)]));
 //BA.debugLineNum = 272;BA.debugLine="Cursor1.Close";
_cursor1.Close();
 //BA.debugLineNum = 273;BA.debugLine="End Sub";
return "";
}
public static String  _write_in_arduino(String _ms) throws Exception{
 //BA.debugLineNum = 321;BA.debugLine="Sub Write_In_Arduino (Ms As String)";
 //BA.debugLineNum = 322;BA.debugLine="Ms = mensaje_Out";
_ms = mostCurrent._mensaje_out;
 //BA.debugLineNum = 323;BA.debugLine="astreams.Write(Ms.GetBytes(\"UTF8\"))";
_astreams.Write(_ms.getBytes("UTF8"));
 //BA.debugLineNum = 324;BA.debugLine="End Sub";
return "";
}
}
