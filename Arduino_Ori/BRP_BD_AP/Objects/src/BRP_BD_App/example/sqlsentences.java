package BRP_BD_App.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class sqlsentences {
private static sqlsentences mostCurrent = new sqlsentences();
public static Object getObject() {
    throw new RuntimeException("Code module does not support this method.");
}
 public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.sql.SQL _sql1 = null;
public BRP_BD_App.example.main _main = null;
public BRP_BD_App.example.starter _starter = null;
public static class _sqlsentences{
public boolean IsInitialized;
public boolean first;
public void Initialize() {
IsInitialized = true;
first = false;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static String  _createtables(anywheresoftware.b4a.BA _ba) throws Exception{
 //BA.debugLineNum = 14;BA.debugLine="Sub CreateTables()";
 //BA.debugLineNum = 18;BA.debugLine="SQL1.ExecNonQuery(\"CREATE TABLE IF NOT EXISTS";
_sql1.ExecNonQuery("CREATE TABLE IF NOT EXISTS Table1 (Box TEXT , Decal_PN TEXT, Figure TEXT, Ca TEXT, Number TEXT, Number_2 TEXT, Model TEXT, QR_Req TEXT)");
 //BA.debugLineNum = 22;BA.debugLine="End Sub";
return "";
}
public static String  _fillsimpledata(anywheresoftware.b4a.BA _ba) throws Exception{
 //BA.debugLineNum = 25;BA.debugLine="Sub FillSimpleData";
 //BA.debugLineNum = 26;BA.debugLine="SQL1.ExecNonQuery(\"INSERT INTO table1 VALUES('";
_sql1.ExecNonQuery("INSERT INTO table1 VALUES('abc', 1, 2)");
 //BA.debugLineNum = 27;BA.debugLine="SQL1.ExecNonQuery2(\"INSERT INTO table1 VALUES(";
_sql1.ExecNonQuery2("INSERT INTO table1 VALUES(?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{(Object)("def"),(Object)(3),(Object)(4)}));
 //BA.debugLineNum = 28;BA.debugLine="End Sub";
return "";
}
public static String  _init(anywheresoftware.b4a.BA _ba) throws Exception{
 //BA.debugLineNum = 9;BA.debugLine="Sub init ()";
 //BA.debugLineNum = 11;BA.debugLine="SQL1.Initialize( File.DirDefaultExternal, \"Decal";
_sql1.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirDefaultExternal(),"Decals1.db",anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 13;BA.debugLine="End Sub";
return "";
}
public static String  _insert(anywheresoftware.b4a.BA _ba,String _box,String _decal_pn,String _figure,String _cat,String _number,String _number_2,String _model,String _qr_req) throws Exception{
 //BA.debugLineNum = 58;BA.debugLine="Sub insert (Box As String, Decal_PN As String, Fig";
 //BA.debugLineNum = 59;BA.debugLine="SQL1.ExecNonQuery2(\"INSERT INTO table1 VALUES(?";
_sql1.ExecNonQuery2("INSERT INTO table1 VALUES(?, ?, ?, ?, ?, ?, ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{(Object)(_box),(Object)(_decal_pn),(Object)(_figure),(Object)(_cat),(Object)(_number),(Object)(_number_2),(Object)(_model),(Object)(_qr_req)}));
 //BA.debugLineNum = 61;BA.debugLine="End Sub";
return "";
}
public static String  _insertmanyrows(anywheresoftware.b4a.BA _ba) throws Exception{
int _i = 0;
 //BA.debugLineNum = 46;BA.debugLine="Sub InsertManyRows";
 //BA.debugLineNum = 47;BA.debugLine="SQL1.BeginTransaction";
_sql1.BeginTransaction();
 //BA.debugLineNum = 48;BA.debugLine="Try";
try { //BA.debugLineNum = 49;BA.debugLine="For i = 1 To 500";
{
final int step3 = 1;
final int limit3 = (int) (500);
for (_i = (int) (1) ; (step3 > 0 && _i <= limit3) || (step3 < 0 && _i >= limit3); _i = ((int)(0 + _i + step3)) ) {
 //BA.debugLineNum = 50;BA.debugLine="SQL1.ExecNonQuery2(\"INSERT INTO table1";
_sql1.ExecNonQuery2("INSERT INTO table1 VALUES ('def', ?, ?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{(Object)(_i),(Object)(_i)}));
 }
};
 //BA.debugLineNum = 52;BA.debugLine="SQL1.TransactionSuccessful";
_sql1.TransactionSuccessful();
 } 
       catch (Exception e8) {
			(_ba.processBA == null ? _ba : _ba.processBA).setLastException(e8); //BA.debugLineNum = 54;BA.debugLine="Log(LastException.Message)";
anywheresoftware.b4a.keywords.Common.Log(anywheresoftware.b4a.keywords.Common.LastException(_ba).getMessage());
 };
 //BA.debugLineNum = 56;BA.debugLine="SQL1.EndTransaction";
_sql1.EndTransaction();
 //BA.debugLineNum = 57;BA.debugLine="End Sub";
return "";
}
public static String  _logtable1(anywheresoftware.b4a.BA _ba) throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor1 = null;
String _dr1 = "";
String _dr2 = "";
String _dr3 = "";
int _i = 0;
 //BA.debugLineNum = 30;BA.debugLine="Sub LogTable1";
 //BA.debugLineNum = 31;BA.debugLine="Dim Cursor1 As Cursor";
_cursor1 = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Public dr1, dr2, dr3 As String";
_dr1 = "";
_dr2 = "";
_dr3 = "";
 //BA.debugLineNum = 33;BA.debugLine="Cursor1 = SQL1.ExecQuery(\"SELECT col1, col2, c";
_cursor1.setObject((android.database.Cursor)(_sql1.ExecQuery("SELECT col1, col2, col3 FROM table1")));
 //BA.debugLineNum = 34;BA.debugLine="For i = 0 To Cursor1.RowCount - 1";
{
final int step4 = 1;
final int limit4 = (int) (_cursor1.getRowCount()-1);
for (_i = (int) (0) ; (step4 > 0 && _i <= limit4) || (step4 < 0 && _i >= limit4); _i = ((int)(0 + _i + step4)) ) {
 //BA.debugLineNum = 35;BA.debugLine="Cursor1.Position = i";
_cursor1.setPosition(_i);
 //BA.debugLineNum = 36;BA.debugLine="Log(\"************************\")";
anywheresoftware.b4a.keywords.Common.Log("************************");
 //BA.debugLineNum = 37;BA.debugLine="Log(Cursor1.GetString(\"col1\"))";
anywheresoftware.b4a.keywords.Common.Log(_cursor1.GetString("col1"));
 //BA.debugLineNum = 38;BA.debugLine="Log(Cursor1.Getstring(\"col2\"))";
anywheresoftware.b4a.keywords.Common.Log(_cursor1.GetString("col2"));
 //BA.debugLineNum = 39;BA.debugLine="Log(Cursor1.Getstring(\"col3\"))";
anywheresoftware.b4a.keywords.Common.Log(_cursor1.GetString("col3"));
 //BA.debugLineNum = 40;BA.debugLine="dr1 = Cursor1.GetString(\"col1\")";
_dr1 = _cursor1.GetString("col1");
 //BA.debugLineNum = 41;BA.debugLine="dr2 = Cursor1.GetString(\"col2\")";
_dr2 = _cursor1.GetString("col2");
 //BA.debugLineNum = 42;BA.debugLine="dr3 = Cursor1.GetString(\"col3\")";
_dr3 = _cursor1.GetString("col3");
 }
};
 //BA.debugLineNum = 44;BA.debugLine="Cursor1.Close";
_cursor1.Close();
 //BA.debugLineNum = 45;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 3;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 6;BA.debugLine="Dim SQL1 As SQL";
_sql1 = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 7;BA.debugLine="Type SQLSentences(first As Boolean)";
;
 //BA.debugLineNum = 8;BA.debugLine="End Sub";
return "";
}
public static String  _read(anywheresoftware.b4a.BA _ba) throws Exception{
 //BA.debugLineNum = 62;BA.debugLine="Sub read";
 //BA.debugLineNum = 63;BA.debugLine="End Sub";
return "";
}
}
