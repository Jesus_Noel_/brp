//#include <Adafruit_TLC5947.h>
#include "Adafruit_TLC5947.h"
#include "Timer.h"
// How many boards do you have chained?
#define NUM_TLC5974 1

#define data1   8
#define clock1   9
#define latch1   10
#define data2   5
#define clock2   6
#define latch2   7
#define oe  -1  // set to -1 to not use the enable pin (its optional)
bool  Col = false;
bool row = false; 
int led(48);
int Binarios;
String Lectura;
int i;
int clamp = 53;
int columna, renglon, dato, dato_old, columna_8;
int salidas[57] = {1, 8, 15, 22, 29, 36, 43, 2, 9, 16, 23, 30, 37, 44, 3, 10, 17, 24, 31, 38, 45, 4, 11, 18, 25, 32, 39, 46, 5, 12, 19, 26, 33, 40, 47, 6, 13, 20, 27, 34, 41, 48, 7, 14, 21, 28, 35, 42, 49, 50, 51, 52, 53, 54, 55, 56, 0};
//int salidas[50] = {1, 7, 14, 21, 28, 35, 42, 2, 8, 15, 22, 29, 36, 43, 3, 9, 16, 23, 30, 37, 44, 4, 10, 17, 24, 31, 38, 45, 5, 11, 18, 25, 32, 39, 46, 6, 12, 19, 26, 33, 40, 47, 7, 13, 20, 27, 34, 41, 48, 0};
int j;
char* userInput;
char* userInput_toRefresh;
char* userInput_toRefresh_Last;
bool isok = false;
Timer t;
static boolean needPrompt = true;
char* serialString()





{
  static char str[21]; // For strings of max length=20
  if (!Serial.available()) return NULL;
  delay(64); // wait for all characters to arrive
  memset(str, 0, sizeof(str)); // clear str
  byte count = 0;
  while (Serial.available())
  {
    char c = Serial.read();
    if (c >= 32 && count < sizeof(str) - 1)
    {
      str[count] = c;
      count++;
    }
  }
  str[count] = '\0'; // make it a zero terminated string
  return str;
}
Adafruit_TLC5947 tlc = Adafruit_TLC5947(NUM_TLC5974, clock1, data1, latch1);
Adafruit_TLC5947 tlc2 = Adafruit_TLC5947(NUM_TLC5974, clock2, data2, latch2);

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  //Serial.begin(9600);
  tlc.begin();
  tlc2.begin();
  t.every(1000, Refresh_Led);
  
  //pin del clamp
  pinMode(clamp, OUTPUT);

  //pins de expansion
  for (i = 0; i < 7; i++){
    pinMode((i + 1), OUTPUT);
  }
  
  
  
  if (oe >= 0) {
    pinMode(oe, OUTPUT);
    digitalWrite(oe, LOW);
  }
  for (i = 0; i < 24; i++) {
    tlc.setPWM(i, 0);
    tlc.write();
  }
  for (i = 0; i < 24; i++) {
    tlc.setPWM(i, 500);
    tlc.write();
    delay(60);
    tlc.setPWM(i, 0);
    tlc.write();
  }
  delay(150);
  for (i = 0; i < 24; i++) {
    tlc2.setPWM(i, 0);
    tlc2.write();
  }
  for (i = 0; i < 24; i++) {
    tlc2.setPWM(i, 500);
    tlc2.write();
    delay(60);
    tlc2.setPWM(i, 0);
    tlc2.write();
  }
    //pin de expansion init
  for (i = 0; i < 7; i++){
   digitalWrite((i+1),HIGH);
   digitalWrite((i+1),LOW);
  }
  
  DDRA = 0;
  DDRC = 0;
  DDRL = 0;
}

// the loop function runs over and over again forever
void loop() {
 //static boolean needPrompt = true;
 //char* userInput;
 // int j;

  if (needPrompt)
  {
    needPrompt = false;
  }
    userInput = serialString(); 
  
   if ((userInput != NULL) && (needPrompt == false))
  {
      for (i = 0; i < 24; i++) {
      tlc.setPWM(i, 0);
      tlc.write();
      tlc2.setPWM(i, 0);
      tlc2.write();
    }

    //Reset Extension
  for (i = 0; i < 7; i++){
    digitalWrite((i+1),LOW);
  }
    
    
    delay(10);
    userInput_toRefresh = userInput;
    
  }
 
 //agregar puerto para agregar columna
 
  columna = PINC;
  renglon = PINA;
  columna_8 =PINL;
//Un puerto para las 7 columnas principales o el otro para columna expansion 
Serial.println(columna_8);
  if(((columna >0) && (renglon >0)) ||(columna_8 >0)){
  switch (renglon) {
    case 64:
      renglon = 1;
      break;
    case 32:
      renglon = 2;
      break;
    case 16:
      renglon = 3;
      break;
    case 8:
      renglon = 4;
      break;
    case 4:
      renglon = 5;
      break;
    case 2:
      renglon = 6;
      break;
     case 1:
      renglon = 7;
      break;
  }

 switch (columna) {
    case 64:
      columna = 7;
      break;
    case 32:
      columna = 6;
      break;
    case 16:
      columna = 5;
      break;
    case 8:
      columna = 4;
      break;
    case 4:
      columna = 3;
      break;
    case 2:
      columna = 2;
      break;
     case 1:
      columna = 1;
  }
  //Expansion de la columna
   switch (columna_8) {
     case 1:
      columna = 8;
      renglon = 1;
      
      Serial.println("Esta es la columna 8");
      break;
      case 2:
      columna = 8;
      renglon = 2;
      
      Serial.println("Esta es la columna 8");
      break;
      case 4:
      columna = 8;
      renglon = 3;
      
      Serial.println("Esta es la columna 8");
      break;
      case 8:
      columna = 8;
      renglon = 4;
      
      Serial.println("Esta es la columna 8");
      break;
      case 16:
      columna = 8;
      renglon = 5;
      
      Serial.println("Esta es la columna 8");
      break;
      case 32:
      columna = 8;
       renglon = 6;
      Serial.println("Esta es la columna 8");
      break;
      case 64:
      columna = 8;
      renglon = 7;
      Serial.println("Esta es la columna 8");
      break;
     
  }
if (Col == true){
  Serial.print(columna);
  Serial.print(" , ");
  Serial.print(renglon);
}
/*
else if (row==true){
  Serial.print(columna);
  Serial.print(" , ");
  Serial.print(renglon);
}*/
else{
    dato = renglon + (columna - 1) * 7;
   
    if (dato != dato_old) {
      if ((salidas[dato-1]) > 7) {
        Serial.print(salidas[dato-1]-1);
      }
      else if  ((salidas[dato-1]) == 7){

      }
      else{
         Serial.print(salidas[dato-1]);
      }
 
     dato_old = dato;

    }
  }
  }
   delay(100);
t.update();
}

void Refresh_Led(){

    
    //apagamos todos


    i = 0;
    while (userInput_toRefresh[i] != '\0') {
      j = userInput_toRefresh[i];
      
     //  Serial.println(j);
       
        if (j > 90) {
        tlc.setPWM(j - 97, 0);
        tlc.write();          
        tlc.setPWM(j - 97, 250);
        tlc.write();
        //clampear
      }
     
    else if (j==63){
      for (i = 0; i < 24; i++) {
        tlc.setPWM(i, 0);
        tlc.write();
        tlc2.setPWM(i, 0);
        tlc2.write();
       
    }
      }
          else if (j==58){
         userInput[i] != '\0';
         Col=true;
      }
           else if (j==59){
         userInput[i] != '\0';
         Col=false;
      }
           else if (j=='Z'){
        
          digitalWrite(clamp, HIGH);
          
      } 
      else if (j=='Y'){
        
          digitalWrite(clamp, LOW);
      }

      //si es entre 1 y 7 o 49-55 en ASCII(valores para expansion)
      else if ((j < 49) && (j > 55)){
        //Prender Digital Out Pin j - 48
        digitalWrite((j-48), LOW);
        digitalWrite((j-48), HIGH);
        
      }
      

      // If j== caracter especifico
      //desclampear por reset o acabando proceso
      
      else {
        //    Serial.println(j - 65);
        tlc2.setPWM(j - 65, 0);
        tlc2.write();        
        tlc2.setPWM(j - 65, 250);
        tlc2.write();
        //clampear

      }
      i++;
     

    }
    i = 0;
 
    needPrompt = true;

  

 
}
